package Snake;
public class Point {
	public int x;  // x value on x axis
	public int y;  // y value on y axis
	
	/**
	 * Constructor; sets and tracks the x and y coordinates of a snake's body parts.
	 * @param x
	 * @param y
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}