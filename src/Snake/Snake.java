package Snake;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.CopyOnWriteArrayList;

public class Snake {
	private Move move;								// help track its moves
	private Point head;
	private Point tail;
	private CopyOnWriteArrayList<Point> points; 	// all points excluding head and tail
	private boolean isAlive;						// tracks it being alive or not

	/**
	 * Get the living status of the snake.
	 * @return Dead or alive.
	 */
	public synchronized boolean getStatus() {
		return isAlive;
	}

	/**
	 * Get the snake's current move.
	 * @return current move
	 */
	public synchronized Move getMove() {
		return move;
	}
	
	/**
	 * Set the coordinates of its next move.
	 * @param move Next move's coordinates.
	 */
	public synchronized void setMove(Move move) {
		this.move = move;
	}

	/**
	 * Get the coordinates of its head.
	 * @return Head's coordinates.
	 */
	public synchronized Point getHead() {
		return head;
	}
	
	/**
	 * Set the coordinates of its head.
	 * @param head
	 */
	public synchronized void setHead(Point head) {
		this.head = head;
	}

	/**
	 * Get the coordinates of its tail.
	 * @return Tail's coordinates.
	 */
	public synchronized Point getTail() {
		return tail;
	}
	
	/**
	 * Set the coordinates of its tail.
	 * @param tail
	 */
	public synchronized void setTail(Point tail) {
		this.tail = tail;
	}

	/**
	 * Get the coordinates of the rest of its body.
	 * @return Body's coordinates.
	 */
	public synchronized CopyOnWriteArrayList<Point> getPoints() {
		return points; 
	}
	
	/**
	 * Set the coordinates of the rest of its body.
	 * @param points
	 */
	public synchronized void setPoints(CopyOnWriteArrayList<Point> points) {
		this.points = points;
	}
	
	/**
	 * Constructor.
	 * @param head Position of the snake's head.
	 */
	public Snake(Point head) {
		// Give it its head.
		this.isAlive = true;
		this.head = head;
		
		// Give it its tail.
		if (head.x > 0) {
			this.tail = new Point(head.x - 1, head.y);
			this.move = Move.EAST;
		} else if (head.y > 0) {
			this.tail = new Point(head.x, head.y - 1);
			this.move = Move.SOUTH;
		} else {
			this.tail = new Point(head.x + 1, head.y);
			this.move = Move.SOUTH;
		}
		
		// Instantiate points list.
		this.points = new CopyOnWriteArrayList<Point>();
	}

	/**
	 * Change the direction of snake
	 * No matter bot player or human player, they all call this function
	 * @param input
	 */
	public synchronized void changeDirection(int input) {
		switch(input) {
		case(0):
			if (this.move != Move.WEST) {
				this.move = Move.EAST;
			}
			break;
		case(1):
			if (this.move != Move.SOUTH) {
				this.move = Move.NORTH;
			}
			break;
		case(2):
			if (this.move != Move.NORTH) {
				this.move = Move.SOUTH;
			}
			break;
		case(3):
			if (this.move != Move.EAST) {
				this.move = Move.WEST;
			}
			break;
		}
	}

	/**
	 * Grow the snake after it's eaten some fruit.
	 */
	public void grow() {
		// analyze the tail direction
		Point secondLast = null;
		
		// Change the current head to the first point of its body.
		if (points.size() == 0) {
			secondLast = head;
		} else {
			secondLast = points.get(points.size() - 1);
		}
		
		// Directions for it to grow.
		int dx = secondLast.x - tail.x;
		int dy = secondLast.y - tail.y;
		Point temp;
		
		// Grow it depending on the directions.
		if (dx > 0) {
			// point left
			// add current tail to body list and generate a new tail
			temp = new Point(tail.x - 1, tail.y);
		} else if (dx < 0) {
			// point right
			temp = new Point(tail.x + 1, tail.y);
		} else if (dy < 0) {
			// point down
			temp = new Point(tail.x, tail.y + 1);
		} else {
			// point up
			temp = new Point(tail.x, tail.y - 1);
		}
		
		// Add tail to points list and make tail the new point.
		points.add(tail);
		tail = temp;
	}

	/**
	 * Change the snake's direction.
	 */
	public synchronized void moveAlong() {
		// Coordinates of the head to move.
		Point newHead = null;
		
		// Perform movement.
		if (move == Move.EAST) {
			newHead = new Point(head.x + 1, head.y);
		} else if (move == Move.WEST) {
			newHead = new Point(head.x - 1, head.y);
		} else if (move == Move.NORTH) {
			newHead = new Point(head.x, head.y - 1);
		} else {
			newHead = new Point(head.x, head.y + 1);
		}

		// Capture head's previous position.
		Point oldHead = head;
		
		// set new head.
		head = newHead;
		// set new Tail.
		if (points.size() == 0) {
			tail = oldHead;
		} else {
			points.add(0, oldHead);
			tail  = points.remove(points.size() - 1);
		}
	}

	/**
	 * Kill off the snake once it's collided into another snake.
	 */
	public void die() {
		if (isAlive) {
			isAlive = false;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		Snake other = (Snake) o;
		
		return head.x == other.head.x &&
				head.y == other.head.y &&
				tail.y == other.tail.y &&
				tail.x == other.tail.x;
	}
}