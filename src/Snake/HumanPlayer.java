package Snake;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class HumanPlayer extends Player {
	private Buffer buffer;
	private Scanner in;

	public HumanPlayer(Buffer buffer, Scanner in) {
		super();
		this.buffer = buffer;
		this.in = in;
	}

	public void logIn() throws IOException {
		LogInfo pInfo = new LogInfo();
		System.out.println("Enter username to login");
		String username = in.nextLine();
		System.out.println("Enter password to login");
		String password = in.nextLine();;
		pInfo.setUsername(username);
		pInfo.setPassword(password);
		pInfo.setPlayer((HumanPlayer)this);
		buffer.push(pInfo);
	}

	public synchronized void handle() {
		// for human player, handle login
		try {
			logIn();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			wait();
		} catch (InterruptedException e) {}

		if (!super.loggedIn) {
			handle();
		}
	}
}