package Snake;
public class LogInfo {
    private HumanPlayer player = null;
    private String username;
    private String password;

    public LogInfo() {}
    
    public LogInfo(HumanPlayer player, String username, String password) {
        this.player = player;
        this.username = username;
        this.password = password;
    }
    
    public String getPassword() {
        return password;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public void setPlayer(HumanPlayer p) {
        this.player = p;
    }
    
    public HumanPlayer getPlayer() {
        return player;
    }
}
