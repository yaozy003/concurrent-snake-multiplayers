package Snake;
import org.mapdb.*;
import java.util.concurrent.ConcurrentMap;
import java.io.*;
import java.util.HashMap;
import java.lang.IllegalArgumentException;

public class SnakeDB{
	private ConcurrentMap<String, String> players;	// player data stored in the database

	/**
	 * Constructor.
	 * Puts accounts into the database, then closes it for later use.
	 */
	public SnakeDB() {
		DB db = DBMaker.fileDB("snakeDB.db").fileMmapEnable().make();
		players = db.hashMap("map", Serializer.STRING, Serializer.STRING).createOrOpen();
		players.put("1234", "1234");
		players.put("123", "123");
		players.put("12", "12");
		players.put("12345", "12345");
		db.commit();
		db.close();
	}

	/**
	 * Checks the login information (username and password) typed from the server class, and compares it
	 * with the information in the database.
	 * @param info Server login
	 * @return Successful or unsuccessful login
	 */
	public boolean checkLogin(LogInfo info) {
		boolean res = false;
		DB db = DBMaker.fileDB("snakeDB.db").fileMmapEnable().make();
		players = db.hashMap("map", Serializer.STRING, Serializer.STRING).createOrOpen();
		
		if (players.containsKey(info.getUsername())) {
			if (players.get(info.getUsername()).equals(info.getPassword())) {
				res = true;
			}
		}
		
		db.close();
		return res;
	}
}