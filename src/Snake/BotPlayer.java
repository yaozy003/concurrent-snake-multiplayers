package Snake;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class BotPlayer extends Player {
	private int ID;			// "login" ID
	private Move move;		// tracks its movement

	/**
	 * Constructor.
	 * @param snake
	 */
	public BotPlayer(Snake snake) {
		super();
		super.setSnake(snake);
	}

	/**
	 * Handles the running thread to control its random movement.
	 */
	public void handle() {
		// for bot player, handle the movement changing
		while (snake.getStatus()) {
			this.randomMove();
			
			try {
				Thread.sleep(ThreadLocalRandom.current().nextInt(20,5000));
			} catch (InterruptedException e) {}
		}
	}
	/**
	 * Randomly generate number between 0 and 3, then update the belonging snake's move
	 */
	public void randomMove() {
		Random rand = new Random();
		int randomInt = rand.nextInt(4);
		super.snake.changeDirection(randomInt);
	}

	/**
	 * Get its login ID.
	 * @return login ID
	 */
	public int getId() {
		return this.ID;
	}

	/**
	 * Gets its move status.
	 * @return Move
	 */
	public Move getMove() {
		return this.move;
	}
}