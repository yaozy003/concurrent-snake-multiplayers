package Snake;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// create buffer with an empty info list, maximum 2 login at one time
		Buffer buffer = new Buffer(2);
		// create server based on the buffer
		Server server = new Server(buffer);
		Thread sthread = new Thread(server);
		sthread.start();

		Scanner in = new Scanner(System.in);
		
		for (int i = 0; i < 4; ++i) {
			Player p = new HumanPlayer(buffer, in);
			Thread pthread = new Thread(p);
			pthread.start();
			
			try {
				pthread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		in.close();
		
		while(true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
