package Snake;

public abstract class Player implements Runnable{

	protected Snake snake;
	protected Boolean loggedIn = false;

	public synchronized void setLoginStatus(Boolean status) {
		loggedIn = status;
	}

	public synchronized boolean getLoginStatus() {
		return loggedIn;
	}

	public synchronized void setSnake(Snake snake) {
		this.snake = snake;
	}

	public synchronized Snake getSnake() {
		return snake;
	}

	public void login() {
		// DO NOTHING
	}

	public void run() {
		handle();
	}

	public abstract void handle();

}
