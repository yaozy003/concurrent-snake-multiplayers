package Snake;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;

public class Game implements KeyListener, WindowListener, Runnable {
	public Server server; // Maintain a connection between the game interface and the server
	private CopyOnWriteArrayList<Player> players;	 // Player list
	private CopyOnWriteArrayList<Point> fruitPoints; // fruit list
	private Player player;							 // each human player has its own game interface
	private int gameGrid[][];						 // Game grid for snakes to move on
	public int gameSize = 100;						 // Size of the game grid.
	private int height = 900;						 // Window height
	private int width = 900;						 // Window width
	private Frame frame = null;						 // Window
	private Canvas canvas = null;					 // Canvas
	private Graphics graph = null;					 // Produces visuals
	private BufferStrategy strategy = null;			 // Organises memory inside frame.
	private boolean gameOver = false;				 // Is the game over or not?

	// time units
	private int minutes, seconds, milliseconds = 0;

	/**
	 * Sets player to be added to the player list.
	 * @param player
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * Get window height.
	 * @return height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Get window width.
	 * @return width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Constructor for the game class to be used in the server.
	 * @param players Players playing the game.
	 * @param player Individual players.
	 * @param fruitPoints Points.
	 */
	public Game(CopyOnWriteArrayList<Player> players, Player player, CopyOnWriteArrayList<Point> fruitPoints) {
		this.players = players;
		this.player = player;
		this.fruitPoints = fruitPoints;
		
		// Initialise frame and canvas.		
		frame = new Frame();
		canvas = new Canvas();

		// Establish size and look of the game.  Current implementation based on assignment code.
		frame.setSize(height, width);
		frame.setResizable(false);
		frame.setLocationByPlatform(true);
		canvas.setSize(height, width);
		canvas.addKeyListener(this);
		frame.addWindowListener(this);
		frame.dispose();
		frame.validate();
		frame.setTitle("Snake");
		frame.setVisible(true);
		frame.add(canvas);
		canvas.setIgnoreRepaint(true);
		canvas.setBackground(Color.BLACK);
		canvas.createBufferStrategy(2);
	}

	public void run() {
		renderGame();
	}

	/**
	 * Renders the game on screen.
	 */
	public void renderGame() {
		// Game does not render if frame is not showing.
		if (!frame.isShowing()) {
			return;
		};
		
		// Instantiate BufferStrategy.
		strategy = canvas.getBufferStrategy();
		
		// Tracks each pixel of the game.
		int gridUnit = height / gameSize;
		
		// While rendered...
		while(true) {
			// Acquite BufferStrategy.
			graph = strategy.getDrawGraphics();
			
			// Draw Background.
			graph.setColor(Color.BLACK);
			graph.fillRect(0, 0, width, height);
			
			// Draw snake.
			for (Player p : players) {
				graph.setColor(Color.BLUE);
				Snake s = p.getSnake();
				
				// Yellow for playing player; blue for other players and bot players.
				if (s.equals(player.getSnake())) {
					graph.setColor(Color.YELLOW);
				} else {
					graph.setColor(Color.BLUE);
				}
				
				// If snake dies, continue.
				if (!s.getStatus()) {
					continue;
				}
				
				// But if it's alive, draw its head, body and tail.
				graph.fillOval(s.getHead().x * gridUnit, s.getHead().y * gridUnit, gridUnit, gridUnit);
				
				for (Point point : s.getPoints()) {
					graph.fillOval(point.x * gridUnit, point.y * gridUnit, gridUnit, gridUnit);
				}
				
				graph.fillOval(s.getTail().x * gridUnit, s.getTail().y * gridUnit, gridUnit, gridUnit);
			}
			
			// draw fruit
			for (Point f: fruitPoints){
				graph.setColor(Color.RED);
				graph.fillOval(f.x * gridUnit, f.y * gridUnit, gridUnit, gridUnit);
			}
			
			// Draw text.
			graph.setFont(new Font(Font.SANS_SERIF, Font.BOLD, height / 60));
			
			// When the game is over.
			if (gameOver) {
				graph.setColor(Color.RED);
				graph.drawString("GAME OVER", height / 2 - 50, height / 2 - 20);
				graph.drawString("YOUR TIME : " + getTime(), height / 2 - 65, height / 2 + 40);
			}

			// Time elapsed.
			graph.setColor(Color.WHITE);
			graph.drawString("TIME = " + getTime(), 20, 20);
			
			// Clock.
			graph.dispose();

			// Draw image from buffer.
			strategy.show();
			Toolkit.getDefaultToolkit().sync();
		}
	}

	/**
	 * Grab the time.
	 * @return How much time has passed.
	 */
	public String getTime() {
		String time = new String(minutes + ":" + seconds);

		if (!gameOver) {
			milliseconds++;

			if (milliseconds == 340) {
				seconds++;
				milliseconds = 0;
			}

			if (seconds == 60) {
				minutes++;
				seconds = 0;
			}
		}

		return time;
	}

	/**
	 * Changes game state to it being game over.
	 */
	public void gameOver() {
		gameOver = true;
	}

	// IMPLETEMENTIONED FUNCTION(S)
	public void keyPressed(KeyEvent ke) {
		int code = ke.getKeyCode();
		switch(code) {
			case KeyEvent.VK_LEFT:
				player.getSnake().changeDirection(3);
				break;
			case KeyEvent.VK_RIGHT:
				player.getSnake().changeDirection(0);
				break;
			case KeyEvent.VK_UP:
				player.getSnake().changeDirection(1);
				break;
			case KeyEvent.VK_DOWN:
				player.getSnake().changeDirection(2);
				break;
			
			// USE TO QUIT WHEN TESTING!!
			case KeyEvent.VK_Q:
				System.exit(1);
		}
	}

	// UNUSED FUNCTIONS
	public void keyTyped(KeyEvent ke) {}
	public void keyReleased(KeyEvent ke) {}
	public void windowOpened(WindowEvent we) {}
	public void windowClosed(WindowEvent we) {}
	public void windowIconified(WindowEvent we) {}
	public void windowDeiconified(WindowEvent we) {}
	public void windowActivated(WindowEvent we) {}
	public void windowDeactivated(WindowEvent we) {}
	public void windowClosing(WindowEvent e) {}
}
