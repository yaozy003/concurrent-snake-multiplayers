package Snake;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.Random;

public class Server implements Runnable {
	private Game game = null;
	private int gameIndex = 0;
	public int GAME_SPEED = 500;					 // tracks game speed
	private volatile long time = 0;					 // helps to let the thread relax a bit
	private long cycleTime = 0;
	private long sleepTime = 0;
	private CopyOnWriteArrayList<Player> players;	 // player list
	private CopyOnWriteArrayList<Game> games;		 // game views list
	private CopyOnWriteArrayList<Point> fruitPoints; // fruits list
	private BufferedWriter movesBuffer;
	private BufferedWriter userBuffer;
	private boolean firstGame = true;
	private Buffer buffer;
	private SnakeDB db;
	private ExecutorService botPlayerPool;

	/**
	 * Constructor.
	 * @param buffer Server buffer.
	 */
	public Server(Buffer buffer) {
		this.buffer = buffer;
		games = new CopyOnWriteArrayList<>();
		fruitPoints = new CopyOnWriteArrayList<>();
		db = new SnakeDB();
		players = new CopyOnWriteArrayList<>();
	}

	/**
	 * Gets the players from the list of players playing the game.
	 * @return Players.
	 */
	public CopyOnWriteArrayList<Player> getPlayers() {
		return players;
	}

	/**
	 * Grabs the buffer.
	 * @return buffer
	 */
	public Buffer getBuffer() {
		return buffer;
	}

	/**
	 * Creates a fruit object and sets it onto the map.
	 */
	private void createAndAddFuirt() {
		// add a fruit
		Point f = null;
		
		// Check occupied points before setting fruit down.
		do {
			int x = ThreadLocalRandom.current().nextInt(0, 100);
			int y = ThreadLocalRandom.current().nextInt(0, 100);
			f = new Point(x, y);
		} while(pointOccupied(f));
		
		fruitPoints.add(f);
	}
	/**
	 * Create a snake that does not overlap other snakes or fruit
	 * @return Snake
	 */
	private Snake createSnake() {
		Point toCheck = null;
		Snake snake = null;
		
		// Look for empty points before placing each snake onto the game grid.
		do {
			do {
				int x = ThreadLocalRandom.current().nextInt(0, 100);
				int y = ThreadLocalRandom.current().nextInt(0, 100);
				toCheck = new Point(x, y);
			} while (pointOccupied(toCheck));
			snake = new Snake(toCheck);
		} while (pointOccupied(snake.getTail()));
		
		return snake;
	}

	/**
	 * Check if the given point is occupied in the game
	 * @param toCheck Check if point is occupied.
	 * @return Is point occupied or not?
	 */
	public boolean pointOccupied(Point toCheck) {
		for (Player p: players) {
			synchronized (p.getSnake()) {
				if (p.getSnake().getHead().x == toCheck.x && p.getSnake().getHead().y == toCheck.y) {
					return true;
				}
				if (p.getSnake().getTail().x == toCheck.x && p.getSnake().getTail().y == toCheck.y) {
					return true;
				}
				for (Point point: p.getSnake().getPoints()) {
					if (point.x == toCheck.x && point.y == toCheck.y)
						return true;
				}
				for (Point point: fruitPoints) {
					if (point.x == toCheck.x && point.y == toCheck.y)
						return true;
				}
			}
		}
		
		return false;
	}

	/**
	 * Checks the login details against the SnakeDB, and logs the player in if
	 * details match.
	 */
	public void checkLogin() {
		LogInfo newInfo = null;
		
		// make sure there are 4 human players at most
		if (gameIndex > 3) {
			return;
		}
		
		// Go through the buffer to pop logged in players out.
		while ((newInfo = buffer.pop()) != null) {
			Player newPlayer = newInfo.getPlayer();
			boolean loggedIn = db.checkLogin(newInfo);
			
			
			// When player is logged in, add them to the game.
			if (loggedIn) {
				// if login succeeds, add the player to players list and create a snake for them.
				newPlayer.setLoginStatus(true);
				newPlayer.setSnake(createSnake());
				players.add(newPlayer);
				
				/* Create a new game view and assign the player into it
				 * In the real world, it'd be accessed by another computer.
				 * Here, it just provides an additional window with the same game state but with different controlled snake. */
				game = new Game(players, newPlayer, fruitPoints);
				
				games.add(game);
				gameIndex++;
			} else {
				// Player did not successfully log in.
				newPlayer.setLoginStatus(false);
				System.out.println("Login detail incorrect, Please login again!");
			}
			
			/* Notify server of new human player.  This helps to keep the game state the same for each
			 * new player.  More detail on this is in the documentation. */
			synchronized (newPlayer) {
				newPlayer.notifyAll();
			}
			
			// After notifications, start new game thread.
			if (loggedIn) {
				Thread gthread = new Thread(game);
				gthread.start();
				
				botPlayerPool = Executors.newFixedThreadPool(100);
				
				// Check if it's the first game of the server.
				if (firstGame) {
					// add fruit and bot players once the first human player joins in.
					firstGame = false;
					
					// add 10 fruit objects.
					for (int i = 0; i < 10; ++i) {
						createAndAddFuirt();
					}
					
					// Create 100 Bot Players with snakes and add them to the players list.
					for (int i = 0; i <= 100; i++) {
						Player bp = new BotPlayer(createSnake());
						players.add(bp);
						
						Thread b = new Thread(bp);
						botPlayerPool.execute(b);
					}
				}
			}
		}
	}

	/**
	 * Runs the server thread (and the server itself).
	 */
	public void run() {
		// constantly check all the players' snakes' states
		while (true) {
			// first, check the buffer to see if there are any new players joining in
			checkLogin();

			// then apply physics logic to each player(snake)
			if (players.size() > 0) {
				for (int i = players.size() - 1; i >= 0; i--) {
					Job job = new Job(players.get(i).getSnake(), this);
					
					// check if any one of them ate fruit
					if (job.checkFruit(fruitPoints)) {
						// Make the game go faster with each piece of fruit eaten to make it more exciting.
						if (GAME_SPEED > 100)
							GAME_SPEED -= 2;
						
						createAndAddFuirt();
					}
					
					// check if snake is alive or not.
					if (!job.checkLife()) {
						players.remove(i);
					} else {
						players.get(i).getSnake().moveAlong();
					}
					
					// One snake survived; end the game.
					if (players.size() == 1) {
						endGame();
					}
				}
			}
			
			// try to sleep to match time
			time = time + 1;
			cycleTime = System.currentTimeMillis() - cycleTime;
			sleepTime = GAME_SPEED - cycleTime;
			
			if (sleepTime > 0) {
				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException ex) {
					System.out.println("Sleep was interrupted.");
				}
			}
		}
	}

	/**
	 * Sets a snake to a player.
	 * @param snake
	 * @param player
	 */
	public void setSnake(Snake snake, Player player) {
		player.snake = snake;
	}

	public BufferedReader readBuffer() {
		return null;
	}
	
	public void endGame() {
		game.gameOver();
	}

	public void updateGame() {
		
	}
}
