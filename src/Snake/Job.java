package Snake;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class Job {
	/*Be specific to individual snake*/
	private Snake snake;
	private Server server;

	/**
	 * Constructor
	 * @param snake Snake being kept track of.
	 * @param server The server it's playing on.
	 */
	public Job(Snake snake, Server server) {
		this.snake = snake;
		this.server = server;
	}

	/**
	 * Checks if the snake has collided with the fruit on the game grid.
	 * @param fruitPoints Fruit coordinates.
	 * @return Did it eat the fruit or not?
	 */
	public boolean checkFruit(CopyOnWriteArrayList<Point> fruitPoints) {
		if (fruitPoints.size() > 0) {
			for (int i = fruitPoints.size() - 1; i > 0; --i) {
				// check if the head of snake overlaps the existing fruit position
				if (fruitPoints.get(i).x == snake.getHead().x && fruitPoints.get(i).y == snake.getHead().y) {
					// remove the eaten fruit
					fruitPoints.remove(i);
					// grow the snake
					snake.grow();
					return true;
				}
				
				for (Point p: snake.getPoints()) {
					if (fruitPoints.get(i).x == p.x && fruitPoints.get(i).y == p.y) {
						// remove the eaten fruit
						fruitPoints.remove(i);
						// grow the snake
						snake.grow();
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Checks if the snake is alive or not after collision.
	 * @return Dead or alive?
	 */
	public boolean checkLife() {
		// check if head touches the walls
		if (snake.getHead().x > 100 - 1)
			snake.setHead(new Point(snake.getHead().x - 100, snake.getHead().y));
		if (snake.getHead().x < 0)
			snake.setHead(new Point(snake.getHead().x + 100, snake.getHead().y));
		if (snake.getHead().y > 100 - 1)
			snake.setHead(new Point(snake.getHead().x, snake.getHead().y - 100));
		if (snake.getHead().y < 0)
			snake.setHead(new Point(snake.getHead().x, snake.getHead().y + 100));

		// check if head touches its own body or others body
		for (Player p: server.getPlayers()) {
			CopyOnWriteArrayList<Point> oneBody = (CopyOnWriteArrayList<Point>) p.getSnake().getPoints().clone();
			
			if (!snake.equals(p.getSnake())) {
				oneBody.add(p.getSnake().getHead());
				oneBody.add(p.getSnake().getTail());
			}
			
			for (Point point: oneBody) {
				if (snake.getHead().x == point.x && snake.getHead().y == point.y) {
					snake.die();
					return false;
				}
			}
		}
		return true;
	}
}
