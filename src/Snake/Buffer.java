package Snake;
import java.util.ArrayList;

public class Buffer {
    public ArrayList<LogInfo> infos;
    private int max;

    public Buffer(int max) {
        this.infos = new ArrayList<>();
        this.max = max;
    }

    public synchronized LogInfo pop() {
        boolean toNotify = false;

        if (infos.size() >= max) {
            toNotify = true;
        }

        if (infos.size() != 0) {
            LogInfo res = infos.remove(0);
            if (toNotify) {
                notifyAll();
            }
            return res;
        }
        return null;
    }

    public synchronized boolean push(LogInfo item) {
        while (infos.size() >= max) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("Something wrong in waiting for pushing");
            }
        }
        infos.add(item);
        return false;
    }
}
