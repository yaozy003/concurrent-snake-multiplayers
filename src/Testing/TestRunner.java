package Testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) @Suite.SuiteClasses({
	BufferTest.class,
	CollisionTest.class,
	MainTest.class,
})
public class TestRunner {}