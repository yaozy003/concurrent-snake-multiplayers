package Testing;
import java.io.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;

import Snake.*;

public class MainTest {
    @Test
    public void main() throws IOException, InterruptedException {
        System.out.println("main");
        String[] args = null;
        
        // Enter scanner details into console automatically.
        final InputStream original = System.in;
        ByteArrayInputStream in = new ByteArrayInputStream("1234\n1234\n123\n123\n12\n12\n12345\n12345".getBytes());
        System.setIn(in);
        Main.main(args);
        System.setIn(original);
    }
    
    @Test
    public void snakeMovementTest() {
		int x = ThreadLocalRandom.current().nextInt(0, 100);
		int y = ThreadLocalRandom.current().nextInt(0, 100);
		Point head = new Point(x, y);
        Snake s = new Snake(head);
        s.setMove(Move.EAST);
        Assert.assertEquals(s.getMove(), Move.EAST);
    }
}