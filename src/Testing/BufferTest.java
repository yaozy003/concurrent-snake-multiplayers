package Testing;

import org.junit.Assert;
import org.junit.Test;

import Snake.Buffer;
import Snake.LogInfo;


public class BufferTest {
	@Test
	public void PushAndPopTest() {
		Buffer buffer = new Buffer(1);
		LogInfo event = new LogInfo(null, "Username", "Password");
		buffer.push(event);
		Assert.assertEquals(event, buffer.pop());
	}
	
	@Test
	public void PushAndPopMultipleTest() {
		int count = 5;
		Buffer buffer = new Buffer(5);
		
		LogInfo[] events = new LogInfo[count];
		for (int i = 0; i < count; i++) {
			events[i] = new LogInfo(null, "Username", "Password");
			buffer.push(events[i]);
		}

		for (int i = 0; i < count; i++) {
			Assert.assertEquals(events[i], buffer.pop());
		}
	}
	
	@Test
	public void PushAndPullNullTest() {
		Buffer buffer = new Buffer(2);
		// appending null should have no effect
		buffer.push(null);
		LogInfo event = new LogInfo(null, "Username", "Password");
		buffer.push(event);
		Assert.assertEquals(null, buffer.pop());
	}
}