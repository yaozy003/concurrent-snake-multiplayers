package Testing;
import Snake.*;

import java.util.concurrent.CopyOnWriteArrayList;

import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.Mockito.*;
import org.mockito.Matchers.*;

public class CollisionTest {
	// Instantiate global JUnit testing objects.
	Buffer buffer = new Buffer(2);
	Server server = new Server(buffer);
	Snake snake1 = new Snake(new Point(5, 5));
	Snake snake2 = new Snake(new Point(6, 6));
	Job job1 = new Job(snake1, server);
	Job job2 = new Job(snake2, server);
	
	// Instantiate global Mock testing objects.
	Snake mockSnake1 = Mockito.mock(Snake.class);
	Snake mockSnake2 = Mockito.mock(Snake.class);
	Job mockJob1 = Mockito.mock(Job.class);
	Job mockJob2 = Mockito.mock(Job.class);
	CopyOnWriteArrayList<Point> fruits = new CopyOnWriteArrayList<Point>();
	
	@Test
	public void collisionTest() {
		snake1.changeDirection(0);
		snake2.changeDirection(1);
		
		/* In game, checkLife method would kill the snake on its own since it calls
		 * for the snake's die method.
		 * But given the circumstances, call the die method here. */
		job1.checkLife();
		snake1.die();
		job2.checkLife();
		snake2.die();
		
		Assert.assertFalse(snake1.getStatus());
		Assert.assertFalse(snake2.getStatus());
	}
	
	@Test
	public void mockCollisionTest() {
		// They're about to bump into each other.
		Mockito.doNothing().when(mockSnake1).changeDirection(0);
		Mockito.doNothing().when(mockSnake2).changeDirection(1);
		mockSnake1.changeDirection(0);
		mockSnake2.changeDirection(1);
		
		// BUMP
		Mockito.when(mockJob1.checkLife()).thenReturn(false);
		Mockito.when(mockJob2.checkLife()).thenReturn(false);
		mockJob1.checkLife();
		mockJob2.checkLife();
		Mockito.when(mockSnake1.getStatus()).thenReturn(false);
		Mockito.when(mockSnake2.getStatus()).thenReturn(false);
		mockSnake1.getStatus();
		mockSnake1.getStatus();
	}
	
	@Test
	public void mockEatTest() {
		// Snake sees fruit so it goes towards it.
		Mockito.doNothing().when(mockSnake1).changeDirection(1);
		mockSnake1.changeDirection(1);
		
		// Snake eats fruit.  Grow it.
		Mockito.when(mockJob1.checkFruit(fruits)).thenReturn(true);
		mockJob1.checkFruit(fruits);
	}
}
